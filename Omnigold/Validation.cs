﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Omnigold
{
    public interface IValidateEntity
    {
        List<DbValidationError> ValidationResult();
    }

    public partial class Customer : IValidateEntity
    { 
        public List<DbValidationError> ValidationResult()
        {
            var list = new List<DbValidationError>();
            Regex nameReg = new Regex(@"^[A-Z][A-Za-z\-]{1,25}$");
            Regex phoneReg = new Regex(@"^\([0-9]{3}\)[0-9]{3}\-[0-9]{4}");
            Regex emailReg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@[a-zA-Z0-9\-]+\.[a-zA-Z]{2,4}$");
            if (!nameReg.IsMatch(FirstName))
            {
                list.Add(new DbValidationError("FirstName", "First name can only be letters and hyphens(-) and the first letter must be capitalized."));
            }
            if (MiddleName != "")
            {
                if (!nameReg.IsMatch(MiddleName))
                {
                    list.Add(new DbValidationError("MiddleName", "Middle name can only be letters and hyphens(-) and the first letter must be capitalized."));
            }
            }
            if (!nameReg.IsMatch(LastName))
            {
                list.Add(new DbValidationError("LastName", "Last name can only be letters and hyphens(-) and the first letter must be capitalized."));
            }
            if(!phoneReg.IsMatch(PhoneNo))
            {
                list.Add(new DbValidationError("PhoneNo", "Phone number must be presented in this format: (###)###-###"));
            }
            if (!emailReg.IsMatch(Email))
            {
                list.Add(new DbValidationError("Email", "Please enter a valid Email."));
            }
            if (DOB > DateTime.Now)
            {
                list.Add(new DbValidationError("DOB", "Please enter a date of birth that has already passed."));
            }
            return list;
        }
    }
    public partial class Employee : IValidateEntity
    {
        public List<DbValidationError> ValidationResult()
        {
            var list = new List<DbValidationError>();
            Regex nameReg = new Regex(@"^[A-Z][A-Za-z\-]{1,25}$");
            Regex phoneReg = new Regex(@"^\([0-9]{3}\)[0-9]{3}\-[0-9]{4}");
            Regex emailReg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@[a-zA-Z0-9\-]+\.[a-zA-Z]{2,4}$");
            Regex addressReg = new Regex(@"^[0-9]{1,5} [A-Za-z' \-]{1,45}$");
            Regex countryReg = new Regex(@"[A-Za-z \-]{1,25}$");
            Regex usernameReg = new Regex(@"^[a-zA-Z0-9\-\._]{5,20}");
            Regex passwordReg = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,20}$");
            Regex postalReg = new Regex(@"^[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] [0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]$");
            if (!nameReg.IsMatch(FirstName))
            {
                list.Add(new DbValidationError("FirstName", "First name can only be letters and hyphens(-) and the first letter must be capitalized."));
            }
            if (MiddleName != "")
            {
                if (!nameReg.IsMatch(MiddleName))
                {
                    list.Add(new DbValidationError("MiddleName", "Middle name can only be letters and hyphens(-) and the first letter must be capitalized."));
                }
            }
            if (!nameReg.IsMatch(LastName))
            {
                list.Add(new DbValidationError("LastName", "Last name can only be letters and hyphens(-) and the first letter must be capitalized."));
            }
            if (!phoneReg.IsMatch(PhoneNo))
            {
                list.Add(new DbValidationError("PhoneNo", "Phone number must be presented in this format: (###)###-###"));
            }
            if (!emailReg.IsMatch(Email))
            {
                list.Add(new DbValidationError("Email", "Please enter a valid Email."));
            }
            if (DOB > DateTime.Now.AddYears(-16))
            {
                list.Add(new DbValidationError("DOB", "Employee must be at least 16 years of age to work in quebec."));
            }
            if(endDate!= null && endDate < startDate)
            {
                list.Add(new DbValidationError("endDate", "End date must be after the employee's start date."));
            }
            if (!addressReg.IsMatch(address))
            {
                list.Add(new DbValidationError("address", "Please enter a valid address"));
            }
            if (!countryReg.IsMatch(Country))
            {
                list.Add(new DbValidationError("Country", "Please enter a country with only letters, spaces and or hyphens"));
            }
            if (!countryReg.IsMatch(City))
            {
                list.Add(new DbValidationError("City", "Please enter a city with only letters, spaces and or hyphens"));
            }
            if (!countryReg.IsMatch(Province))
            {
                list.Add(new DbValidationError("Province", "Please enter a Province with only letters, spaces and or hyphens"));
            }
            
            if(Global.ctx.Employees.Where(a=>a.username==username).FirstOrDefault()!= null)
            {
                list.Add(new DbValidationError("username", "The username has already been taken."));
            }
            if (!usernameReg.IsMatch(username))
            {
                list.Add(new DbValidationError("username", "The username can only contain letters, numbers, underscore, periods and hyphens"));
            }
            if (!passwordReg.IsMatch(empPassword))
            {
                list.Add(new DbValidationError("empPassword", "The password must be 8-20 characters long and contain at least one number,\none uppercase and one lower case letter and no special characters."));
            }
            if (!postalReg.IsMatch(PostalCode))
            {
                list.Add(new DbValidationError("PostalCode", "The postal code must be in a format like follows: A1A 1A1"));
            }


            return list;
        }
    }
    public partial class Payment : IValidateEntity
    {
        public List<DbValidationError> ValidationResult()
        {
            var list = new List<DbValidationError>();
            if (PaymentDate> DateTime.Now)
            {
                list.Add(new DbValidationError("PaymentDate", "Only add a payment once it has been processed."));
            }

                if (Amount * 100 != Math.Floor((decimal)Amount * 100))
                {
                    list.Add(new DbValidationError("Amount", "The amount can only have 2 decimal points"));
                }

            if (AdditionalCost != null)
            {
                if (AdditionalCost * 100 != Math.Floor((decimal)AdditionalCost * 100))
                {
                    list.Add(new DbValidationError("AdditionalCost", "The Additional cost can only have 2 decimal points"));
                }
            }
            if (Discount >= 100)
            {
                list.Add(new DbValidationError("Discount", "The Discount is a percent and cannot be over 100%"));
            }



            return list;
        }
    }

    public partial class Reservation : IValidateEntity
    {
        public List<DbValidationError> ValidationResult()
        {
            var list = new List<DbValidationError>();
            if (CheckIn > CheckOut)
            {
                list.Add(new DbValidationError("CheckOut", "The check out date has to be later than check in date."));
            }
            if(Room == null)
            {
                return list;
            }

            foreach (Reservation r in Room.Reservations)
            {
                if(r.CheckIn<CheckIn &&r.CheckOut> CheckIn)
                {
                    list.Add(new DbValidationError("Room", "The Room is already booked for a period of your stay."));
                    break;
                }
                if (CheckOut>r.CheckIn && CheckOut<r.CheckOut)
                {
                    list.Add(new DbValidationError("Room", "The Room is already booked for a period of your stay."));
                    break;
                }
            }
            

            return list;
        }
    }

    public partial class OmniGoldEntities : DbContext
    {
        protected override DbEntityValidationResult ValidateEntity(DbEntityEntry entityEntry, IDictionary<object, object> items)
        {
            IValidateEntity entityValidation = entityEntry.Entity as IValidateEntity;
            List< DbValidationError> list = entityValidation.ValidationResult();
            if (list.Count == 0) { return base.ValidateEntity(entityEntry, items); }

            return new DbEntityValidationResult(entityEntry, list);
            
        }
    }
}
