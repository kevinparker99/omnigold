﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Omnigold.CustomMessageBox;

namespace Omnigold
{
    /// <summary>
    /// Interaction logic for Reservation.xaml
    /// </summary>
    public partial class Reservation : UserControl
    {
        public Reservation()
        {
            InitializeComponent();
        }

        private void btnUpdRes_Click(object sender, RoutedEventArgs e)
        {
            Reservation selres = (sender as Button).DataContext as Reservation;
            if (selres.Payment != null)
            {
                new CustomMessageBox("You can not change a reservation that has been paid for.", MessageType.Error, MessageButtons.Ok).ShowDialog();
                return;
            }
            
            if (selres.CheckIn < DateTime.Now)
            {
                new CustomMessageBox($"You can only change the end of reservation since the reservation has already started.", MessageType.Warning, MessageButtons.Ok).ShowDialog();

            }
            Window dlg = new ReservationWiz(selres);
            if (dlg.ShowDialog() == true)
            {
                lvReservation.ItemsSource = Global.ctx.Reservations.Where(r => r.PaymentID == null).ToList();
            }

        }



        private void btnReservation_Click(object sender, RoutedEventArgs e)
        {
            Window dlg = new ReservationWiz();
           if( dlg.ShowDialog()== true)
            {
                lvReservation.ItemsSource = Global.ctx.Reservations.Where(r => r.PaymentID == null).ToList();
            }          
        }

        private void btnDelRes_Click(object sender, RoutedEventArgs e)
        {
            Reservation selres = (sender as Button).DataContext as Reservation;
            if (selres.CheckIn < DateTime.Now && selres.hasCheckedIn)
            {
                new CustomMessageBox("Cannot Delete a reservation that is in progress or that has finished.", MessageType.Error, MessageButtons.Ok).ShowDialog();
                return;
            }

            try
            {

                if (new CustomMessageBox("Are you sure you want to delete this reservation ?", MessageType.Warning, MessageButtons.OkCancel).ShowDialog() == true)
                {


                    Global.ctx.Reservations.Attach(selres);
                    Global.ctx.Reservations.Remove(selres);
                    Global.ctx.SaveChanges();
                    lvReservation.ItemsSource = Global.ctx.Reservations.ToList();
                    new CustomMessageBox("Record Deleted", MessageType.Success, MessageButtons.Ok).ShowDialog();
                }
                else
                {
                    new CustomMessageBox("Record Deletion Canceled", MessageType.Info, MessageButtons.Ok).ShowDialog();
                    return;
                }
            }
            catch (SystemException ex)
            {
                new CustomMessageBox($"{ex.Message} \n Error in Delete Data", MessageType.Error, MessageButtons.Ok).ShowDialog();
            }
        }

        private void btnPayRes_Click(object sender, RoutedEventArgs e)
        {
            Reservation selres = (sender as Button).DataContext as Reservation;
            if(selres.PaymentID!= null)
            {
                new CustomMessageBox($"This reservation has already been paid for.", MessageType.Error, MessageButtons.Ok).ShowDialog();
            }
            Window pay = new AddPaymentDlg(selres);
            pay.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            if(pay.ShowDialog() == true)
            {
            }

        }


        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(this.Visibility == Visibility.Visible)
            {
                lvReservation.ItemsSource = Global.ctx.Reservations.Where(r=> r.PaymentID==null).ToList();
            }
            if (Global.loginEmp.position == Position.Cleaner || Global.loginEmp.position == Position.Bellhop)
            {
                btnAdd.IsEnabled = false;
            }
        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            lvReservation.ItemsSource = Global.ctx.Reservations.Where(r => r.CustFirst.Contains(tbSearch.Text) || r.CustLast.Contains(tbSearch.Text) || r.RoomNum.Contains(tbSearch.Text) || r.CheckIn.ToString().Contains(tbSearch.Text) || r.CheckOut.ToString().Contains(tbSearch.Text)).ToList();
        }
    }
}
