//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Omnigold
{
    using System;
    
    public enum Position : short
    {
        Owner = 1,
        Admin = 2,
        Bellhop = 3,
        Receiptionist = 4,
        Cleaner = 5
    }
}
