﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Omnigold.CustomMessageBox;

namespace Omnigold
{
    /// <summary>
    /// Interaction logic for HomeWindow.xaml
    /// </summary>
    public partial class HomeWindow : UserControl
    {
        public HomeWindow()
        {
            try
            {
                //Global.ctx = new OmniGoldEntities();
                InitializeComponent();
                lvUpcomingReservation.ItemsSource = Global.ctx.Reservations.ToList().Where(r => r.hasCheckedIn == false && DateTime.Now.AddDays(-2) < r.CheckIn && r.CheckIn < DateTime.Now.AddDays(2)).ToList();
                //lvUpcomingReservation.ItemsSource = Global.ctx.Reservations.ToList();
            }
            catch (SystemException)
            {
                new CustomMessageBox($"You have no Upcoming reservation in the next two days", MessageType.Warning, MessageButtons.Ok).ShowDialog();
            }

            
        }

        private void btnCheckIn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool? Result = new CustomMessageBox("Check In for this Customer?", MessageType.Warning, MessageButtons.OkCancel).ShowDialog();

                if (Result.Value)
                {
                    Button clickedButton = sender as Button;
                    DataView dv = lvUpcomingReservation.ItemsSource as DataView;
                    Reservation selReserv = clickedButton.DataContext as Reservation;
                    
                    selReserv.hasCheckedIn = true;
                    Global.ctx.Entry(selReserv).State = System.Data.Entity.EntityState.Modified;
                    Global.ctx.SaveChanges();


                    new CustomMessageBox($"{selReserv.CustFirst} {selReserv.CustLast} has checked In at {DateTime.Now} at Room {selReserv.RoomNum}.", MessageType.Success, MessageButtons.Ok).ShowDialog();
                    //remove from list 
                    lvUpcomingReservation.ItemsSource = Global.ctx.Reservations.ToList().Where(r => r.hasCheckedIn == false && DateTime.Now.AddDays(-2) < r.CheckIn && r.CheckIn < DateTime.Now.AddDays(2)).ToList();
                    //lvUpcomingReservation.ItemsSource = Global.ctx.Reservations.Where(r => r.hasCheckedIn == false).ToList();
                    // new CustomMessageBox($"{selReserv.CustFirst} has checked In at {DateTime.Now} at Room {selReserv.RoomNum}.", MessageType.Success, MessageButtons.Ok).ShowDialog();

                }
                else
                {
                    return;
                }
            }
            catch (SystemException ex)
            {
                //MessageBox.Show(ex.Message + "Error in Delete Data", "Database Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                new CustomMessageBox($"{ex.Message} \n Error in Check In", MessageType.Warning, MessageButtons.Ok).ShowDialog();
            }
        }

       
    }
}
