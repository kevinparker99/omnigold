﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Omnigold.CustomMessageBox;

namespace Omnigold
{
    /// <summary>
    /// Interaction logic for EmployeeDetail.xaml
    /// </summary>
    public partial class EmployeeDetail : UserControl
    {        
        public EmployeeDetail()
        {
            try
            {
                InitializeComponent();
                lvEmp.ItemsSource = Global.ctx.Employees.ToList();
                //Utils.AutoResizeColumns(lvEmp);
            }
            catch (SystemException ex)
            {
                new CustomMessageBox($"{ex.Message} \n Employee Detail Initialize Component", MessageType.Warning, MessageButtons.Ok).ShowDialog();
                //MessageBox.Show(ex.Message + "Employee Detail Initialize Component", "Database error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }        
        }
        /*
        private bool UserFilter(object item) {
            if (String.IsNullOrEmpty(txtSearch.Text))
                return true;
            else
                return ((item as Employee).FirstName.IndexOf(txtSearch.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }*/
        private void txtSearch_SelectionChanged(object sender, RoutedEventArgs e)
        {
            /*
            if (txtSearch.Text.Length == 0) { //return;  
            }
            else
            {
                CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvEmp.ItemsSource);
                view.Filter = UserFilter;
            }
            if (lvEmp.Items.Count > 0 || txtSearch.Text.Length > 0)
            {
                CollectionViewSource.GetDefaultView(lvEmp.ItemsSource).Refresh();
            }*/
        }
        /*
        public IQueryable<Employee> GetSearchedItems([Control("txtSearch")] string Search) {
            IQueryable<Employee> query = Global.ctx.Employees.OrderBy(emp => emp.FirstName);

            if (!string.IsNullOrEmpty(Search))
            {
                query = query.Where(emp => (emp.FirstName.Contains(Search) || emp.LastName.Contains(Search)));

            }
            return query;
        }*/
        private void btnUpd_Click(object sender, RoutedEventArgs e)
        {
            try
            {                
               // Button clickedUpdBtn = sender as Button;
                Employee selEmpUpd = (sender as Button).DataContext as Employee;
                AddEditEmployeeDlg dlg = new AddEditEmployeeDlg(selEmpUpd);
                Window parentWindow = Window.GetWindow(this);
                dlg.btnAdd.Content = "Update";

                dlg.dtpRetDate.IsEnabled = true;
                if (dlg.ShowDialog() == true)
                {
                    lvEmp.ItemsSource = Global.ctx.Employees.ToList();
                }
            }
            catch (SystemException ex)
            {
                //MessageBox.Show(ex.Message + "Error in Update data", "Database error", MessageBoxButton.OK, MessageBoxImage.Warning);
                new CustomMessageBox($"{ex.Message} \n Error in Update employee data", MessageType.Warning, MessageButtons.Ok).ShowDialog();
            }            
        }

        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool ? result = new CustomMessageBox("Are you sure you want to delete Employee ?", MessageType.Warning, MessageButtons.OkCancel).ShowDialog();
                if(result.Value)
                //if (MessageBox.Show("Are you sure you want to delete ? ", "Delete Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning) == MessageBoxResult.OK)
                {
                    Button clickedButton = sender as Button;
                    DataView dv = lvEmp.ItemsSource as DataView;
                    //DataRowView drv = clickedButton.DataContext as DataRowView;
                    //dv.Table.Rows.Remove(drv.Row);

                    Employee selEmp = clickedButton.DataContext as Employee;
                    
                    Global.ctx.Employees.Attach(selEmp);
                    Global.ctx.Employees.Remove(selEmp);
                    Global.ctx.SaveChanges();
                    //MessageBox.Show("Record Deleted");
                    lvEmp.ItemsSource = Global.ctx.Employees.ToList();
                    new CustomMessageBox("Record Deleted", MessageType.Success, MessageButtons.Ok).ShowDialog();
                }
                else
                {
                    //MessageBox.Show("User Select Cancel for Delete");
                    return;
                }
            }
            catch (SystemException ex)
            {
                new CustomMessageBox($"{ex.Message} \n Error in Delete Data", MessageType.Warning, MessageButtons.Ok).ShowDialog();
            }            
        }
        private void btnAddEmp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddEditEmployeeDlg dlg = new AddEditEmployeeDlg(null);
                Window parentWindow = Window.GetWindow(this);
                dlg.btnAdd.Content = "Add";
               
                if (dlg.ShowDialog() == true)
                {
                    lvEmp.ItemsSource = Global.ctx.Employees.ToList();
                }
            }
            catch (SystemException ex)
            {
                //MessageBox.Show(ex.Message + "Error in Adding Data", "Database Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                new CustomMessageBox($"{ex.Message} \n Error in Adding employee data", MessageType.Warning, MessageButtons.Ok).ShowDialog();
            }            
        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            lvEmp.ItemsSource = Global.ctx.Employees.Where(Emp => Emp.FirstName.Contains(tbSearch.Text) || Emp.MiddleName.Contains(tbSearch.Text) || Emp.LastName.Contains(tbSearch.Text) || Emp.Email.Contains(tbSearch.Text) || Emp.PhoneNo.Contains(tbSearch.Text) || Emp.address.Contains(tbSearch.Text) || Emp.City.Contains(tbSearch.Text) || Emp.Province.Contains(tbSearch.Text) || Emp.Country.Contains(tbSearch.Text)).ToList();
        }
    }
}
