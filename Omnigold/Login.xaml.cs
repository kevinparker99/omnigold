﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Omnigold.CustomMessageBox;
/*
Date : 17-04-2021
Note : Add new login check logic 
 */

namespace Omnigold
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            Global.ctx = new OmniGoldEntities();
            InitializeComponent();
        }
        private void ClearInput() {
            txtPass.Password = "";
            txtUser.Text = "";
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtUser.Text.Length == 0)
                {
                    new CustomMessageBox("Please Enter user name", MessageType.Warning, MessageButtons.Ok).ShowDialog();
                    return;
                }
                if (txtPass.Password.Length == 0)
                {
                    new CustomMessageBox("Please Enter user password", MessageType.Warning, MessageButtons.Ok).ShowDialog();
                    return;
                }
                CheckLogin();
                /*
                var empId = 0;
                var query = (from emp in Global.ctx.Employees where emp.username == txtUser.Text select emp).ToList();
                if (query.Count > 0)
                {
                    empId = query.FirstOrDefault<Employee>().EmpId;
                    Global.loginEmpId = empId;
                    var checkPass = (from emppass in Global.ctx.Employees where emppass.empPassword == txtPass.Password && emppass.EmpId == empId select emppass).ToList();
                    if (checkPass.Count > 0)
                    {
                        var empIdPass = checkPass.FirstOrDefault<Employee>().EmpId;
                        if (empId == empIdPass)
                        {
                            // Employee
                            MainWindow mainDlg = new MainWindow();
                            mainDlg.ShowDialog();
                    this.Visibility = Visibility.Hidden;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid Password");
                        txtPass.Password = "";
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Invalid User");
                    txtUser.Text = "";
                }
                */
            }
            catch (SystemException ex)
            {
                //MessageBox.Show(ex.Message + "Database Error");
                new CustomMessageBox($"{ex.Message} \n Database Error in Login ", MessageType.Warning, MessageButtons.Ok).ShowDialog();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
        public void CheckLogin() {
            //Employee currLoginEmp =(Employee) (from emppass in Global.ctx.Employees where emppass.empPassword == txtPass.Password && emppass.username == txtUser.Text select emppass);
            Employee emp = Global.ctx.Employees.Where(e => e.username == txtUser.Text && e.empPassword == txtPass.Password).FirstOrDefault();
            if(emp == null)
            {
                new CustomMessageBox("Please check username or password", MessageType.Warning, MessageButtons.Ok).ShowDialog();
                ClearInput();
                return;
            }
            if (emp.empPassword == txtPass.Password && emp.username == txtUser.Text)
            {
                var EmpId = emp.EmpId;
                if (EmpId != 0) {
                    MainWindow mainDlg = new MainWindow(emp);
                    this.Visibility = Visibility.Hidden;
                    mainDlg.ShowDialog();
                    ClearInput();
                    this.Visibility = Visibility.Visible;
                    this.Focus();
                }
            }
            else {
                //MessageBox.Show("Please check username or password");
                new CustomMessageBox("Please check username or password", MessageType.Warning, MessageButtons.Ok).ShowDialog();
                ClearInput();
            }
        }
    }
}
