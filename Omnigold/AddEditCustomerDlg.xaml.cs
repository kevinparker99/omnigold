﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Omnigold.CustomMessageBox;

namespace Omnigold
{    
    public partial class AddEditCustomerDlg : Window
    {
        public Customer currCust;

        private void ClearCustInputs()
        {
            tbCustFirstName.Text = "";
            tbCustMiddleName.Text = "";
            tbCustLastName.Text = "";
            tbCustPhone.Text = "";
            tbCustEmail.Text = "";
            dpCustDob.DisplayDate = DateTime.Today;
        }

        public AddEditCustomerDlg(Customer Cust = null)
        {
            try
            {
                InitializeComponent();
                currCust = Cust;
                if (currCust != null)
                {
                    lblCustID.Content = Cust.CustID;
                    tbCustFirstName.Text = Cust.FirstName;
                    tbCustMiddleName.Text = Cust.MiddleName;
                    tbCustLastName.Text = Cust.LastName;
                    tbCustPhone.Text = Cust.PhoneNo;
                    tbCustEmail.Text = Cust.Email;
                    dpCustDob.SelectedDate = Cust.DOB;
                }
            }
            catch (SystemException ex)
            {
                //MessageBox.Show(ex.Message + "Database Error while loading data", "Database Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                new CustomMessageBox($"{ex.Message} \n Database Error while loading data \n Database error", MessageType.Warning, MessageButtons.Ok).ShowDialog();

            }
        }

        private void btAddCustomerWindow_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btAddCustomerWindow_Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {                
                if (currCust == null)
                {

                    Customer newCust = new Customer();
                    newCust.FirstName = tbCustFirstName.Text;
                    newCust.MiddleName = tbCustMiddleName.Text;
                    newCust.LastName = tbCustLastName.Text;
                    newCust.PhoneNo = tbCustPhone.Text;
                    newCust.Email = tbCustEmail.Text;
                    newCust.DOB = dpCustDob.SelectedDate;

                    Global.ctx.Customers.Add(newCust);
                }
                else
                {
                    currCust.FirstName = tbCustFirstName.Text;
                    currCust.MiddleName = tbCustMiddleName.Text;
                    currCust.LastName = tbCustLastName.Text;
                    currCust.PhoneNo = tbCustPhone.Text;
                    currCust.Email = tbCustEmail.Text;
                    currCust.DOB = dpCustDob.SelectedDate.Value;
                    Global.ctx.Entry(currCust).State = System.Data.Entity.EntityState.Modified;
                }

                Global.ctx.SaveChanges();
                ClearCustInputs();
                //MessageBox.Show("Customer Saved");
                new CustomMessageBox("Customer Saved", MessageType.Success, MessageButtons.Ok).ShowDialog();
                DialogResult = true;
            }
            catch (DbEntityValidationException ex) 
            {
                if(currCust== null)
                {
                    Global.ctx.Dispose();
                    Global.ctx = new OmniGoldEntities();
                }

                List<string> errorList = new List<string>();
                foreach (DbEntityValidationResult entityErr in ex.EntityValidationErrors)
                {
                    foreach (DbValidationError error in entityErr.ValidationErrors)
                    {
                        errorList.Add($"{error.ErrorMessage}");
                    }
                }
                int msgBoxHeight = 220 + (errorList.Count*18);
                //MessageBox.Show(String.Join("\n", errorList) + "Error in Adding Data", "Database Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                string displayErrorList = String.Join("\n", errorList);
                new CustomMessageBox($"{displayErrorList} \nError in Adding Data", MessageType.Error, MessageButtons.Ok, msgBoxHeight).ShowDialog();
            }
        }
    }
}
