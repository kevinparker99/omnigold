﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Omnigold.CustomMessageBox;

namespace Omnigold
{

    public partial class ReservationWiz : Window
    {
        Reservation currRes;
        List<Room> availableRoom = new List<Room>();
        public ReservationWiz(Reservation res = null)
        {
            currRes = res;
            InitializeComponent();

            lvCustomer.ItemsSource = Global.ctx.Customers.ToList();
            comboRoomType.ItemsSource = Enum.GetValues(typeof(RoomType)).Cast<RoomType>();
            if (currRes!= null)
            {
                lvCustomer.SelectedItem = currRes.Customer;
                ChooseCustomer.CanSelectNextPage = true;
                ChooseDate.CanSelectNextPage = true;
                ChooseRoom.CanSelectNextPage = true;


                if (currRes.CheckIn < DateTime.Now)
                {
                    btCustomer.IsEnabled = false;
                    lvCustomer.IsEnabled = false;
                    dpCheckIn.IsEnabled = false;
                    dpCheckIn.DisplayDateStart = DateTime.Now;
                }
                dpCheckIn.SelectedDate = currRes.CheckIn;
                dpCheckIn.DisplayDateStart = currRes.CheckIn;
                dpCheckOut.SelectedDate = currRes.CheckOut;



            }
        }

        private void btCustomer_Click(object sender, RoutedEventArgs e)
        {
            Window dlg = new AddEditCustomerDlg();
            
            if(dlg.ShowDialog() == true)
            {
                lvCustomer.ItemsSource = Global.ctx.Customers.ToList();
                lvCustomer.SelectedIndex = lvCustomer.Items.Count-1;
            }


        }

        private void tbName_TextChanged(object sender, TextChangedEventArgs e)
        {
            lvCustomer.ItemsSource = Global.ctx.Customers.Where(c => c.FirstName.Contains(tbName.Text) || c.MiddleName.Contains(tbName.Text) || c.LastName.Contains(tbName.Text)).ToList();
        }

        private void lvCustomer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(lvCustomer.SelectedItem != null)
            {
                ChooseCustomer.CanSelectNextPage = true;
                return;
            }
            ChooseCustomer.CanSelectNextPage = false;
        }

        private void dpCheckOut_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if(dpCheckIn.SelectedDate!= null && dpCheckOut.SelectedDate != null)
            {
                ChooseDate.CanSelectNextPage = true;
                return;
            }
            ChooseDate.CanSelectNextPage = false; 
        }

        private void dpCheckIn_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            dpCheckOut.DisplayDateStart = dpCheckIn.SelectedDate;
            if (dpCheckIn.SelectedDate != null && dpCheckOut.SelectedDate != null)
            {
                ChooseDate.CanSelectNextPage = true;
                return;
            }
            ChooseDate.CanSelectNextPage = false;
        }

        private void Wizard_Next(object sender, Xceed.Wpf.Toolkit.Core.CancelRoutedEventArgs e)
        {
            if (ChooseDate.IsVisible == true)
            {
                availableRoom.Clear();
                if (currRes != null && currRes.CheckIn < DateTime.Now)
                {
                    lvRoom.IsEnabled = false;
                }
                else
                {
                    foreach (Room r in Global.ctx.Rooms)
                    {
                        int check = 0;
                        foreach (Reservation res in r.Reservations)
                        {
                            if (res.CheckIn < dpCheckIn.SelectedDate && dpCheckIn.SelectedDate < res.CheckOut ||
                                res.CheckIn < dpCheckOut.SelectedDate && dpCheckOut.SelectedDate < res.CheckOut ||
                                dpCheckIn.SelectedDate.Value < res.CheckIn && res.CheckOut < dpCheckOut.SelectedDate.Value)
                            {
                                check++;
                                break;
                            }
                        }
                        if (check == 0)
                        {
                            availableRoom.Add(r);
                        }
                    }

                }
                if (currRes != null)
                {
                    availableRoom.Add(currRes.Room);
                }
                lvRoom.ItemsSource = availableRoom;
            }
            else if (ChooseRoom.IsVisible == true)
            {
                lbCheckIn.Content = dpCheckIn.SelectedDate.Value;
                lbCheckOut.Content = dpCheckOut.SelectedDate.Value;
                if (lvCustomer.SelectedItem == null)
                {
                    lbName.Content = $"{currRes.CustFirst} {currRes.CustLast}";
                }
                else
                {
                    lbName.Content = $"{((Customer)lvCustomer.SelectedItem).FirstName} {((Customer)lvCustomer.SelectedItem).LastName}";
                }

                if (lvRoom.SelectedItem == null)
                {
                    lbRoomNo.Content = currRes.RoomNum;
                    lbRoomType.Content = currRes.Room.RoomType;
                }
                else
                {
                    lbRoomNo.Content = ((Room)lvRoom.SelectedItem).RoomNo;
                    lbRoomType.Content = ((Room)lvRoom.SelectedItem).RoomType;
                }

            }
        }

        private void Wizard_Finish(object sender, Xceed.Wpf.Toolkit.Core.CancelRoutedEventArgs e)
        {
            try
            {
                if (currRes == null)
                {

                    Reservation Res = new Reservation();
                    Res.Customer = (Customer)lvCustomer.SelectedItem;
                    Res.CheckIn = dpCheckIn.SelectedDate.Value;
                    Res.CheckOut = dpCheckOut.SelectedDate.Value;
                    Res.Room = (Room)lvRoom.SelectedItem;
                    Res.EmpId = Global.loginEmp.EmpId;
                    Global.ctx.Reservations.Add(Res);
                }
                else
                {
                    currRes.CustId = ((Customer)lvCustomer.SelectedItem).CustID;
                    currRes.CheckIn = dpCheckIn.SelectedDate.Value;
                    currRes.CheckOut = dpCheckOut.SelectedDate.Value;
                    currRes.RoomId= ((Room)lvRoom.SelectedItem).RoomID;
                    currRes.EmpId = Global.loginEmp.EmpId;
                    Global.ctx.Entry(currRes).State = System.Data.Entity.EntityState.Modified;
                }

                Global.ctx.SaveChanges();

                new CustomMessageBox("Reservation Saved", MessageType.Success, MessageButtons.Ok).ShowDialog();
            }
            catch (DbEntityValidationException ex)
            {
                if (currRes == null)
                {
                    Global.ctx.Dispose();
                    Global.ctx = new OmniGoldEntities();
                }
                List<string> errorList = new List<string>();
                foreach (DbEntityValidationResult entityErr in ex.EntityValidationErrors)
                {
                    foreach (DbValidationError error in entityErr.ValidationErrors)
                    {
                        errorList.Add($"{error.ErrorMessage}");
                    }
                }
                int msgBoxHeight = 220 + (errorList.Count * 18);
                string displayErrorList = String.Join("\n", errorList);
                new CustomMessageBox($"{displayErrorList} \nError in Adding Data", MessageType.Error, MessageButtons.Ok, msgBoxHeight).ShowDialog();

            }
        }

        private void comboRoomType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lvRoom.ItemsSource = availableRoom.Where(r => r.RoomType == (RoomType)(comboRoomType.SelectedIndex + 1));
        }

        private void lvRoom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvRoom.SelectedItem != null)
            {
                ChooseRoom.CanSelectNextPage = true;    
                return;
            }
            ChooseRoom.CanSelectNextPage = false;
        }
    }
}
