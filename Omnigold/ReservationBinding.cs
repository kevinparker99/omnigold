﻿namespace Omnigold
{
    public partial class Reservation
    {
        public string RoomNum { get
            {
                return Global.ctx.Rooms.Find(this.RoomId).RoomNo;
            } 
        }
        public string CustFirst
        {
            get
            {
                return Global.ctx.Customers.Find(this.CustId).FirstName;
            }
        }
        public string CustLast
        {
            get
            {
                return Global.ctx.Customers.Find(this.CustId).LastName;
            }
        }
    }
}