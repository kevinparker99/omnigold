﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Validation;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Omnigold
{
    /// <summary>
    /// Interaction logic for AddEditEmployeeDlg.xaml
    /// </summary>
    public partial class AddEditEmployeeDlg : Window
    {
        Employee currEmp;
        public AddEditEmployeeDlg(Employee emp)
        {
            try
            {
                InitializeComponent();
                
                cmbPosition.ItemsSource = Enum.GetValues(typeof(Position));
                
                if (emp == null)
                {
                    cmbPosition.SelectedIndex = 0;
                    dtpStartDate.SelectedDate = DateTime.Now;
                    dtpBirthDate.SelectedDate =  DateTime.Now;
                }
                else
                {
                    currEmp = emp;
                    txtFName.Text = currEmp.FirstName;
                    txtMName.Text = currEmp.MiddleName;
                    txtLName.Text = currEmp.LastName;

                    txtPhone.Text = currEmp.PhoneNo;
                    txtEmail.Text = currEmp.Email;
                    dtpBirthDate.SelectedDate = currEmp.DOB;
                    //txtPosition.Text = 1 + "";//currEmp.Position;
                    cmbPosition.SelectedValue = (Position)currEmp.position;
                    dtpStartDate.SelectedDate = currEmp.startDate;
                    dtpRetDate.SelectedDate = currEmp.endDate;

                    txtAdd.Text = currEmp.address;
                    txtCity.Text = currEmp.City;
                    txtState.Text = currEmp.Province;
                    txtCoutry.Text = currEmp.Country;
                    txtPostCode.Text = currEmp.PostalCode;
                    txtUserName.Text = currEmp.username;
                    txtPass.Text = currEmp.empPassword;
                }
            }
            catch (SystemException ex)
            {
                new CustomMessageBox(ex.Message + "Database Error while loading data\nDatabata Error", CustomMessageBox.MessageType.Error, CustomMessageBox.MessageButtons.Ok).ShowDialog();
            }
            
        }
        bool ValidateInputs()
        {
            List<string> errList = new List<string>();
            if (txtFName.Text.Length < 2 || txtFName.Text.Length > 100) {
                errList.Add("Firstname must be 2-100 char long");
            }
            if (txtLName.Text.Length < 2 || txtLName.Text.Length > 100)
            {
                errList.Add("Last name must be 2-100 char long");
            }
            if (txtPhone.Text.Length == 0) {
                errList.Add("Please enter phone no");
            }
            if(txtEmail.Text.Length == 0)
            {
                errList.Add("Please enter email");
            }
            if(dtpBirthDate.SelectedDate.Value == null)
            {
                errList.Add("Please select Birth Date");
            }

            if (errList.Count > 0) {
                new CustomMessageBox(String.Join("\n", errList), CustomMessageBox.MessageType.Error, CustomMessageBox.MessageButtons.Ok).ShowDialog();
                return false;
            }
            return true;
        }
        private void ClearInputs()
        {
            txtFName.Text = "";
            txtMName.Text = "";
            txtLName.Text = "";
            txtPhone.Text = "";
            txtEmail.Text = "";
            dtpBirthDate.SelectedDate = DateTime.Now;
            
            cmbPosition.SelectedIndex = 0;
            dtpStartDate.SelectedDate = DateTime.Now;
            dtpRetDate.SelectedDate = DateTime.Now;
            txtAdd.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtCoutry.Text = "";
            txtPostCode.Text = "";
            txtUserName.Text = "";
            txtPass.Text = "";
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (currEmp == null)
                {
                    string name = txtFName.Text;
                    string midName = txtMName.Text;
                    string lastName = txtLName.Text;
                    string phone = txtPhone.Text;
                    string email = txtEmail.Text;
                    DateTime birthDate = dtpBirthDate.SelectedDate.Value;
                    //string position = txtPosition.Text;
                    string position = cmbPosition.SelectedItem.ToString();
                    int posi = (cmbPosition.SelectedIndex +1);
                    DateTime startDate = dtpStartDate.SelectedDate.Value;
                    //DateTime endDate = dtpRetDate.SelectedDate.Value; // Not in Add / only for Update 
                    string add = txtAdd.Text;
                    string city = txtCity.Text;
                    string state = txtState.Text;
                    string country = txtCoutry.Text;
                    string postCode = txtPostCode.Text;
                    string userName = txtUserName.Text;
                    string password = txtPass.Text;
                    Employee emp = new Employee
                    {
                        FirstName = name,
                        MiddleName = midName,
                        LastName = lastName,
                        PhoneNo = phone,
                        Email = email,
                        DOB = birthDate,
                        position = (Position)posi, //position,
                        startDate = startDate,
                        address = add,
                        City = city,
                        Province = state,
                        Country = country,
                        PostalCode = postCode,
                        username = userName,
                        empPassword = password
                    };
                    Global.ctx.Employees.Add(emp);
                }
                else
                {
                    dtpRetDate.IsEnabled = true;
                    currEmp.FirstName = txtFName.Text;
                    currEmp.MiddleName = txtMName.Text;
                    currEmp.LastName = txtLName.Text;

                    currEmp.PhoneNo = txtPhone.Text;
                    currEmp.Email = txtEmail.Text;
                    currEmp.DOB = dtpBirthDate.SelectedDate.Value;
                    currEmp.position = (Position)(cmbPosition.SelectedIndex+1) ; //(txtPosition.Text);
                    currEmp.startDate = dtpStartDate.SelectedDate.Value;
                    if (dtpRetDate.SelectedDate != null)
                    {
                        currEmp.endDate = dtpRetDate.SelectedDate.Value;
                    }

                    currEmp.address = txtAdd.Text;
                    currEmp.City = txtCity.Text;
                    currEmp.Province = txtState.Text;
                    currEmp.Country = txtCoutry.Text;
                    currEmp.PostalCode = txtPostCode.Text;
                    currEmp.username = txtUserName.Text;
                    currEmp.empPassword = txtPass.Text;
                    Global.ctx.Entry(currEmp).State = System.Data.Entity.EntityState.Modified;
                }
                Global.ctx.SaveChanges();
                DialogResult = true;
                ClearInputs();
                new CustomMessageBox("Employee added.", CustomMessageBox.MessageType.Success, CustomMessageBox.MessageButtons.Ok).ShowDialog();
            }
            catch (DbEntityValidationException ex)
            {
                if(currEmp== null)
                {
                    Global.ctx.Dispose();
                    Global.ctx = new OmniGoldEntities();
                }

                List<string> errorList = new List<string>();
                foreach (DbEntityValidationResult entityErr in ex.EntityValidationErrors)
                {
                    foreach (DbValidationError error in entityErr.ValidationErrors)
                    {
                        errorList.Add($"Error Property Name {error.PropertyName} : Error Message: {error.ErrorMessage}");
                    }
                }
                new CustomMessageBox(String.Join("\n", errorList), CustomMessageBox.MessageType.Error, CustomMessageBox.MessageButtons.Ok).ShowDialog();
            }
        }
    }
}
