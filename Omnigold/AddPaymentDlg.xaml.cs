﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Omnigold
{
    /// <summary>
    /// Interaction logic for AddPaymentDlg.xaml
    /// </summary>
    public partial class AddPaymentDlg : Window
    {
        decimal amount =0 ;
        decimal addCost = 0;
        decimal dis = 0;
        decimal taxAmt = 0;
        decimal finalAmt = 0;

        Reservation currRes;
        public AddPaymentDlg( Reservation res)
        {
            InitializeComponent();
            currRes = res;
            dtpPayDate.SelectedDate = DateTime.Now;
            txtAmount.Text = Math.Round(currRes.Room.Price * (currRes.CheckOut.Subtract(currRes.CheckIn).Days + 1),2) + "";
            txtTax.Text = Math.Round(currRes.Room.Price * (currRes.CheckOut.Subtract(currRes.CheckIn).Days + 1) * (Decimal).18475, 2) + "";
            txtCustomer.Text = $"{currRes.CustFirst} {currRes.CustLast}";
            txtExpMon.Text = "12";
            txtExpYear.Text = "2021";
            Global.ctx = new OmniGoldEntities();
            CalculateAmount();
        }

        [Obsolete]
        private void btnPay_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                amount = Convert.ToDecimal(txtAmount.Text);
                if (amount > 10000) {
                    MessageBox.Show("Amount must not be greater than 10000");
                    return;
                }
                addCost = Convert.ToDecimal(txtAddCost.Text);
                if (addCost > 500) {
                    MessageBox.Show("Additional Cost must not be greater than 500");
                    return;
                }
                dis = Convert.ToDecimal(txtDiscount.Text);
                if (dis > 450) {
                    MessageBox.Show("Discount amount must not be greater than 450");
                }
                taxAmt = Convert.ToDecimal(txtTax.Text);
                finalAmt = Convert.ToDecimal(txtFinalAmt.Text);
                DateTime payDate = dtpPayDate.SelectedDate.Value;
                               
                StripeConfiguration.SetApiKey("sk_test_51IgDsCF8pIUagn4r7OGPpdfl3dXq2BBkssaGcjXsVZCO9kyhuOMVjgUMvr9jSwYyQDg7E7yHXFtBHwt2DphtVMPK0076mFwwzE");
                var opttoken = new TokenCreateOptions
                {
                    //Card = new CreditCardOptions
                    Card = new TokenCardOptions
                    {
                        Number = txtCardNo.Text,
                        ExpYear = Convert.ToInt32(txtExpYear.Text),
                        ExpMonth = Convert.ToInt32(txtExpMon.Text),
                        Cvc = txtCVV.Text
                    }
                };
                var tokenservice = new TokenService();
                Token stripetoken = tokenservice.Create(opttoken);

                var options = new ChargeCreateOptions
                {
                    //Amount = Convert.ToInt32(txtFinalAmt.Text) * 100,
                    Amount = (Int32)(Convert.ToDecimal(txtFinalAmt.Text) * 100),
                    Currency = "cad",
                    Description = "test transactions",
                    Source = stripetoken.Id
                    //SourceId = stripetoken.Id
                };
                var service = new ChargeService();
                Charge charge = service.Create(options);
                if (charge.Paid)
                {
                    
                    Payment payment = new Payment {  PaymentDate = payDate, CustId =currRes.CustId, Amount = amount, AdditionalCost = addCost, Discount = dis, Tax = taxAmt, FinalAmount = finalAmt, EmpId = currRes.EmpId };
                    Global.ctx.Payments.Add(payment);
                    Global.ctx.SaveChanges();
                    new CustomMessageBox("Payment has been processed.", CustomMessageBox.MessageType.Success, CustomMessageBox.MessageButtons.Ok).ShowDialog();
                    DialogResult = true;
                }
                else
                {
                    //charge.Invoice.Customer;
                    MessageBox.Show("Failed on Sttripe");
                    new CustomMessageBox("Failed to process payment", CustomMessageBox.MessageType.Error, CustomMessageBox.MessageButtons.Ok).ShowDialog();
                }
            }
            catch (SystemException ex)
            {
                new CustomMessageBox($"Database error in save payment\n{ex.Message}", CustomMessageBox.MessageType.Error, CustomMessageBox.MessageButtons.Ok).ShowDialog();
            }
        }
        private void ClearInput() {
            txtAddCost.Text = "";
            txtDiscount.Text = "";
            txtFinalAmt.Text = "";
            txtCardNo.Text = "";
            txtCVV.Text = "";
            txtExpMon.Text = "";
            txtExpYear.Text = "";
        }
        private void CalculateAmount()
        {
            decimal.TryParse(txtAmount.Text, out decimal amt);
            if (amt > 0)
            {
                decimal.TryParse(txtAddCost.Text, out decimal addCost);
                decimal.TryParse(txtDiscount.Text, out decimal dis);

                decimal amtAddDis = amt + addCost - dis;
                decimal taxAmt = Math.Round( amtAddDis * Convert.ToDecimal(18.475 / 100),2);
                txtTax.Text = taxAmt + "";
                finalAmt = amtAddDis + taxAmt;
                txtFinalAmt.Text = Math.Round(finalAmt,2) + "";
            }
        }

        private void txtDiscount_LostFocus(object sender, RoutedEventArgs e)
        {
            CalculateAmount();
        }

        private void txtAddCost_TextChanged(object sender, TextChangedEventArgs e)
        {
            //if(txtAddCost.Text.Length > 0) { CalculateAmount();  }
        }

        private void txtAddCost_LostFocus(object sender, RoutedEventArgs e)
        {
            CalculateAmount();
        }
    }
}
