﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Omnigold.CustomMessageBox;

namespace Omnigold
{    
    public partial class CustomerWindow : UserControl
    {
        public CustomerWindow()
        {
            try
            {
                InitializeComponent();
                lvCustomer.ItemsSource = Global.ctx.Customers.ToList();
            }
            catch (SystemException ex)
            {
                //MessageBox.Show(ex.Message + "Customer Detail Initialize Component", "Database error", MessageBoxButton.OK, MessageBoxImage.Warning);
                new CustomMessageBox($"{ex.Message} \n Customer Detail Initialize Component \n Database error", MessageType.Warning, MessageButtons.Ok).ShowDialog();
            }
        }

        private void btnAddCustomer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                AddEditCustomerDlg dlg = new AddEditCustomerDlg(null);                
                Window parentWindow = Window.GetWindow(this);
                dlg.Owner = parentWindow;
                dlg.btAddCustomerWindow_Add.Content = "Add";
                if (dlg.ShowDialog() == true)
                {
                    lvCustomer.ItemsSource = Global.ctx.Customers.ToList();
                }
            }
            catch (SystemException ex)
            {
                //MessageBox.Show(ex.Message + "Error in Adding Data", "Database Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                new CustomMessageBox($"{ex.Message} \n Error in Adding Data \n Database error", MessageType.Warning, MessageButtons.Ok).ShowDialog();
            }
        }

        private void btnUpdCust_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Customer selCustUpd = (sender as Button).DataContext as Customer;
                AddEditCustomerDlg dlg = new AddEditCustomerDlg(selCustUpd);                
                Window parentWindow = Window.GetWindow(this);
                dlg.Owner = parentWindow;
                dlg.btAddCustomerWindow_Add.Content = "Update";
                if (dlg.ShowDialog() == true)
                {
                    lvCustomer.ItemsSource = Global.ctx.Customers.ToList();
                }
            }
            catch (SystemException ex)
            {
                //MessageBox.Show(ex.Message + "Error in Update data", "Database error", MessageBoxButton.OK, MessageBoxImage.Warning);
                new CustomMessageBox($"{ex.Message} \n Error in Update data \n Database error", MessageType.Warning, MessageButtons.Ok).ShowDialog();
            }
        }

        private void btnDelCust_Click(object sender, RoutedEventArgs e)
        {
            /*
            try
            {
                if (MessageBox.Show("Are you sure you want to delete? ", "Delete Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning) == MessageBoxResult.OK)
                {
                    Button clickedButton = sender as Button;
                    DataView dv = lvCustomer.ItemsSource as DataView;

                    Customer selCust = clickedButton.DataContext as Customer;
                    Global.ctx.Customers.Remove(selCust);
                    Global.ctx.SaveChanges();
                    lvCustomer.ItemsSource = (from c in Global.ctx.Customers select c).ToList<Customer>();
                    MessageBox.Show("Record Deleted");
                }
                else
                {
                    MessageBox.Show("User Select Cancel for Delete");
                    return;
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message + "Error in Delete Data", "Database Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            } */

            try
            {
                bool? Result = new CustomMessageBox("Are you sure you want to delete Customer?", MessageType.Warning, MessageButtons.OkCancel).ShowDialog();

                if (Result.Value)
                {
                    Button clickedButton = sender as Button;
                    DataView dv = lvCustomer.ItemsSource as DataView;

                    Customer selCust = clickedButton.DataContext as Customer;
                    Global.ctx.Customers.Attach(selCust);
                    Global.ctx.Customers.Remove(selCust);
                    Global.ctx.SaveChanges();
                    lvCustomer.ItemsSource = (from c in Global.ctx.Customers select c).ToList<Customer>();
                    //MessageBox.Show("Record Deleted");
                    new CustomMessageBox("Record Deleted", MessageType.Success, MessageButtons.Ok).ShowDialog();
                    
                }
                else
                {
                    return;
                }
            }
            catch (SystemException ex)
            {
                //MessageBox.Show(ex.Message + "Error in Delete Data", "Database Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                new CustomMessageBox($"{ex.Message} \n Error in Delete Data", MessageType.Warning, MessageButtons.Ok).ShowDialog();
            }


        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            lvCustomer.ItemsSource = Global.ctx.Customers.Where(c => c.FirstName.Contains(tbSearch.Text) || c.MiddleName.Contains(tbSearch.Text) || c.LastName.Contains(tbSearch.Text)||c.Email.Contains(tbSearch.Text)|| c.PhoneNo.Contains(tbSearch.Text)).ToList();
        }
    }
}
