﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Omnigold.CustomMessageBox;

namespace Omnigold
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public bool loginOut = false;
        public MainWindow(Employee currLoginEmp = null)
        {
            InitializeComponent();
            Global.loginEmp = currLoginEmp;
            if (Global.loginEmp.position == Position.Cleaner || Global.loginEmp.position == Position.Bellhop)
            {
                btEmployee.IsEnabled = false;
                btCustomer.IsEnabled = false;
            }else if(Global.loginEmp.position == Position.Receiptionist)
            {
                btEmployee.IsEnabled = false;
            }
            
        }

        private void btnPayment_Click(object sender, RoutedEventArgs e)
        {
            AddPaymentDlg payDlg = new AddPaymentDlg(UCReservation = null);
            //payDlg.Owner = this;
            Window parentWindow = Window.GetWindow(this);
            //payDlg.ShowDialog();
        }

        private void btnCustomer_Click(object sender, RoutedEventArgs e)
        {
            /*
            CustomerWindow dlg = new CustomerWindow();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true) //modal
            {
                
            }*/

            CustomerWindow dlg = new CustomerWindow();
            //dlg.Owner = this ;
            Window parentWindow = Window.GetWindow(this);
        }

        private void btnReservation_Click(object sender, RoutedEventArgs e)
        {
            Reservation dlg = new Reservation();
            Window parentWindow = Window.GetWindow(this);
        }
        
        private void btnEmployee_Click(object sender, RoutedEventArgs e)
        {
            EmployeeDetail dlg = new EmployeeDetail();
            //dlg.ShowDialog();
            Window parentWindow = Window.GetWindow(this);
        }


        // ------------------------------- User Control buttons ------------------

        public void setActiveUserControl(UserControl control)
        {
            // Colapse all user controls
            UCHome.Visibility = Visibility.Collapsed;
            UCEmployee.Visibility = Visibility.Collapsed;
            //UCPayment.Visibility = Visibility.Collapsed;
            UCCustomer.Visibility = Visibility.Collapsed;
            UCReservation.Visibility = Visibility.Collapsed;
            
            // Show the current user
            control.Visibility = Visibility.Visible;
        }


        private void btHome_Click(object sender, RoutedEventArgs e)
        {
            setActiveUserControl(UCHome);
        }

        private void btEmployee_Click(object sender, RoutedEventArgs e)
        {
            setActiveUserControl(UCEmployee);
        }

        private void btCustomer_Click(object sender, RoutedEventArgs e)
        {
            setActiveUserControl(UCCustomer);
        }

        private void btReservation_Click(object sender, RoutedEventArgs e)
        {
            setActiveUserControl(UCReservation);
        }

        private void btPayment_Click(object sender, RoutedEventArgs e)
        {
            //setActiveUserControl(UCPayment);
        }

        private void btReports_Click(object sender, RoutedEventArgs e)
        {
            //setActiveUserControl(UCReports);
        }

        private void btLogOut_Click(object sender, RoutedEventArgs e)
        {
            // edit after fix login pagee -----------------------> do not forget
            if ((bool)new CustomMessageBox("Are you sure you want to Log out?", MessageType.Confirmation, MessageButtons.YesNo).ShowDialog())
            {
                loginOut = true;
                Close();
            }
            
        }

        private void btPower_Click(object sender, RoutedEventArgs e)
        {
            Close();


        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            if (!loginOut)
            {
                bool? Result = new CustomMessageBox("Are you sure you want leave OmniGold HMS?", MessageType.Warning, MessageButtons.OkCancel).ShowDialog();

                if (Result.Value)
                {
                    Application.Current.Shutdown();
                }
                else
                {
                    e.Cancel = true;
                }
            }

        }
    }
}
